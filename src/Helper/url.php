<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 21/04/2019
 * Time: 15:24
 */

namespace src\Helper;

/**
 * Class url
 * @package src\Helper
 */
class url
{
    /**
     * @param $url
     * @return string
     */
    public function generateUrl($url){

        return __base_url__.'index.php?url='.$url ;

    }
}