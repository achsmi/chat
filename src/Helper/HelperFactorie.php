<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 21/04/2019
 * Time: 17:51
 */

namespace src\Helper;


use config\parameter;

/**
 * Class HelperFactorie
 * @package src\Helper
 */
class HelperFactorie
{


    public $url ;
    public $parametre ;
    /**
     * HelperFactorie constructor.
     */
    public function __construct()
    {
        $this->url = new url() ;
        $this->parametre = new parameter() ;
    }

    /**
     * @param $rootName
     * @return string
     */
    public function getUrl($rootName){

        $url = new url() ;
        return $url->generateUrl($rootName) ;
    }

    public function geCrfToken($form){

        $url = new CrfToken() ;
        return $url->generateToken($form) ;
    }


    /**
     * @return \PDO
     */
    public  function databaseConnect()
    {
        return DbConnection::getInstance($this->parametre->_host ,  $this->parametre->_user , $this->parametre->_password , $this->parametre->_dbname)->handle() ;
    }


}