<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 21/04/2019
 * Time: 13:16
 */

namespace src\Helper ;

use src\Model\User;
use src\Model\UserRepository;

/**
 * Class Controller
 * @package src\Helper
 */
abstract class Controller
{


    var $helper ;
    var $reposotiry ;
    /**
     * Controller constructor.
     */
    public function __construct()
    {
        session_start();

        $this->helper = new HelperFactorie() ;
        $this->reposotiry = new Repository() ;

    }

    /**
     * @param $view
     */
    public function renderView($view) {

        include_once(__ROOT__.'/'.PROJECT_DIR."/src/Views/".$view.".php");


    }

    /**
     * @param $rootName
     * @return string
     */
    public function generateUrl($rootName){

        return $this->helper->url->generateUrl($rootName);

    }
    public function crfToken($form){

        return $this->helper->geCrfToken($form) ;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function getReposotiry($model) {

        return $this->reposotiry->getManager($model) ;

    }

    /**
     * @param $rootName
     */
    public function redirectTo($rootName){
        header('Location: '.$this->generateUrl($rootName));
        exit();

    }

    /**
     * @return mixed
     */
    public function getUser(){

        if(isset($_SESSION['id'])) return   $this->getReposotiry(UserRepository::class)->find($_SESSION['id']);
        else $this->redirectTo("") ;
    }

    /**
     * @param User $user
     */
    public function connectUser(User $user){

        $this->getReposotiry(UserRepository::class)->updateUser($user);
        $_SESSION['id'] = $user->getId();
        $_SESSION['user'] = $user->getEmail();
      return  $this->redirectTo("chat");
    }

    /**
     * @param User $user
     */
    public function disconnection(User $user){

        $user->setStatus(0) ;
        $this->getReposotiry(UserRepository::class)->updateUser($user);
        $_SESSION['id'] = "";
        $_SESSION['user'] = "";
        session_destroy();

        return  $this->redirectTo("");
    }

}