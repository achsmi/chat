<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 21/04/2019
 * Time: 18:03
 */

namespace src\Helper;


use config\parameter;
use PDO ;

/**
 * Class DbConnection
 * @package src\Helper
 */
class DbConnection
{
    private static $_instance = null;

    private $_host;
    private $_user;
    private $_password;
    private $_dbname;
    private $_handle;

    /**
     * DbConnection constructor.
     * @param $_host
     * @param $_user
     * @param $_password
     * @param $_dbname
     */
    private function __construct($_host , $_user , $_password , $_dbname  )
    {

        $this->_host = $_host;
        $this->_user = $_user;
        $this->_password = $_password;
        $this->_dbname = $_dbname;
        $this->_handle = null;

        try {
            $this->_handle = new PDO("mysql:host=$this->_host;dbname=$this->_dbname", $this->_user, $this->_password);
            $this->_handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('Connection failed or database cannot be selected : ' . $e->getMessage());
        }
    }

    /**
     *
     */

    public function __destruct()
    {
        if (!is_null($this->_handle)) {
            $this->_handle = null;
        }
    }

    /**
     * @param $_host
     * @param $_user
     * @param $_password
     * @param $_dbname
     * @return null|DbConnection
     */
    public static function getInstance( $_host , $_user , $_password , $_dbname)
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self($_host , $_user , $_password , $_dbname);
        }
        return self::$_instance;
    }

    /**
     * @return PDO
     */
    public function handle()
    {
        return $this->_handle;
    }
}