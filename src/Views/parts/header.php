<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>chat En ligne : Achraf</title>

    <!-- Bootstrap core CSS -->
    <link href="web/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="web/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="web/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="web/css/freelancer.min.css" rel="stylesheet">
    <script src="web/vendor/jquery/jquery.min.js"></script>

</head>

<body id="page-top">

<!-- Navigation -->
<?php include __ROOT__.'/'.PROJECT_DIR."/src/Views/parts/menu.php"?>
<section style="margin-top: 80px">
    <div class="container">

<?php  if(isset( $_SESSION['user'])) {?>
    <div class="row">
    <span class="alert alert-info">Bienvenu, <b><?php echo $_SESSION['user']; ?></b></span>

    </div>
<?php }?>
