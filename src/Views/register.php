<?php include __ROOT__ . '/' . PROJECT_DIR . "/src/Views/parts/header.php" ?>



        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-12" style="text-align: center">

                <?php if (isset($this->status)) { ?>
                    <span class="alert alert-danger"><?php echo $this->status ?></span>

                <?php } ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <form name="user-form" action="<?php echo $this->helper->getUrl('registre') ?>" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input name="email" type="email" class="form-control" id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Enter email">

                    </div>
                    <input name="crfToken" type="hidden"  value="<?php echo $this->helper->geCrfToken("registre") ?>"/>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mots de passe</label>
                        <input name="password" type="password" class="form-control" id="exampleInputPassword1"
                               placeholder="Password">
                    </div>

                    <button type="submit" class="btn btn-primary">Sauvegarder</button>
                </form>

            </div>
        </div>




<?php include __ROOT__ . '/' . PROJECT_DIR . "/src/Views/parts/footer.php" ?>



