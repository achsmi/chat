<?php include __ROOT__ . '/' . PROJECT_DIR . "/src/Views/parts/header.php" ?>


<div class="row" style="">
    <div class="col-md-9">
        <div class="form-group">
                    <textarea name="content" class="form-control" id="content"
                              placeholder="tchat"></textarea>

        </div>
    </div>
    <div class="col-md-3">
        <button type="button" OnClick="sendMessage()" class="btn btn-primary">Envoyer</button>

    </div>
</div>
<div class="row">
    <div class="col-md-8">

        <ul id="chatbox" class="list-group messages">
            <?php foreach ($this->messages as $message) { ?>

                <li class="list-group-item">


                    <div class="row">
                        <div class="col-md-8"><strong><?php echo $message->getUser()->getEmail() ?></strong>
                        </div>
                        <div class="col-md-4"><?php echo $message->getPublicationDate() ?>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <?php echo $message->getContent() ?>
                        </div>
                    </div>


                </li>
            <?php } ?>
        </ul>

    </div>
    <div class="col-md-4 users">

        <ul id="userbox" class="list-group ">
            <?php foreach ($this->users as $user) { ?>

                <li class="list-group-item">

                    <span class="-pull-left"><?php echo $user->getEmail() ?></span>
                    <span class="-pull-right"><?php echo $user->getStatus() == 1 ? "<span class='badge badge-success'>Active</span>" : "<span class='badge badge-danger'>Inactive</span>" ?></span>

                </li>
            <?php } ?>

        </ul>

    </div>
</div>


<script>

    function sendMessage() {
        message = $('#content').val();
        if (message != "") {
            $.ajax({
                url: "<?php echo $this->helper->getUrl('sendMessage') ?>",
                type: "POST",
                data: {message: message},
                success: function (html) {
                    $("#chatbox").html(html); //Insert chat log into the #chatbox div
                    $('#content').val("");
                },

            });
        }
    }


    setInterval(function () {
        getUsers()

        getMessages();
    }, 2000);

    setInterval(function () {
        imConnected()
    }, 500);

    setInterval(function () {
        logoutAll()
    }, 20000);



    function logoutAll(){
        $.ajax({
            url: "<?php echo $this->helper->getUrl('logoutAll') ?>",
            type: "POST",
            success: function (html) {
            },

        });

    }
    function imConnected(){
        $.ajax({
            url: "<?php echo $this->helper->getUrl('imConnected') ?>",
            type: "POST",
            success: function (html) {
            },

        });
    }
    function getMessages() {
        $.ajax({
            url: "<?php echo $this->helper->getUrl('getMessages') ?>",
            success: function (html) {
                $("#chatbox").html(html); //Insert chat log into the #chatbox div

            },

        });
    }

    function getUsers() {
        $.ajax({
            url: "<?php echo $this->helper->getUrl('getUsers') ?>",
            success: function (html) {
                $("#userbox").html(html); //Insert chat log into the #chatbox div

            },

        });
    }


</script>


<?php include __ROOT__ . '/' . PROJECT_DIR . "/src/Views/parts/footer.php" ?>



