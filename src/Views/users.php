<?php foreach ($this->users as $user) { ?>

    <li class="list-group-item">

        <span class="-pull-left"><?php echo $user->getEmail() ?></span>
        <span class="-pull-right"><?php echo $user->getStatus() == 1 ? "<span class='badge badge-success'>Active</span>" : "<span class='badge badge-danger'>Inactive</span>" ?></span>

    </li>
<?php } ?>