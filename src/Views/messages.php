
<?php foreach ($this->messages as $message) { ?>

    <li class="list-group-item">


        <div class="row">
            <div class="col-md-8"><strong><?php echo $message->getUser()->getEmail() ?></strong>
            </div>
            <div class="col-md-4"><?php echo $message->getPublicationDate() ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-8">
                <?php echo $message->getContent() ?>
            </div>
        </div>


    </li>
<?php } ?>