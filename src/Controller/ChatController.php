<?php

namespace src\Controller ;



use src\Helper\Controller;
use src\Helper\url;
use src\Model\Message;
use src\Model\MessageRepository;
use src\Model\UserRepository;


class ChatController extends Controller
{

    var $url ;

    /**
     * chat online
     */

    public function indexAction()
    {
        $this->user =  $this->getUser() ;

        $this->messages = $this->getReposotiry(MessageRepository::class)->liste();
        $this->users = $this->getReposotiry(UserRepository::class)->liste();

        $this->renderView("chat");
    }

    /**
     * message List
     */
    public function listAction(){
        $this->messages = $this->getReposotiry(MessageRepository::class)->all();
        $this->renderView("messagesList");
    }

    /**
     * Add Message
     */
    public function sendMessageAction(){

        if (isset($_POST['message'])) {

            $message  = new Message();
            $message->setContent(htmlspecialchars(strip_tags($_POST['message'])));
            $message->setUser($this->getUser()) ;

            List($this->status, $message) = $this->getReposotiry(MessageRepository::class)->save($message);
        }

        $this->getMessagesAction() ;


    }


    public function getMessagesAction(){

        $this->messages = $this->getReposotiry(MessageRepository::class)->liste();
        $this->renderView("messages");

    }




}