<?php

namespace src\Controller;


use src\Helper\Controller;
use src\Helper\url;
use src\Model\User;
use src\Model\UserRepository;

class UserController extends Controller
{

    var $url;

    /**
     * user List
     */
    public function indexAction()
    {

        $this->users = $this->getReposotiry(UserRepository::class)->liste();
        $this->renderView("userListe");

    }

    /**
     *
     */
    public function getUsersAction()
    {
        $this->users = $this->getReposotiry(UserRepository::class)->liste();
        $this->renderView("users");
    }

    /**
     * Login Action
     */
    public function loginAction()
    {


        if (isset($_POST['email']) and isset($_POST['password']) and isset($_POST['crfToken'])) {

            if ($_POST['crfToken'] == $this->crfToken("login")) {

                $user = new User();
                $user->setEmail(htmlspecialchars(strip_tags($_POST['email'])));
                $user->setPassword(htmlspecialchars(strip_tags($_POST['password'])));
                List($this->status, $user) = $this->getReposotiry(UserRepository::class)->login($user);
                if ($this->status == "success") {
                    $this->connectUser($user);
                }
            }
        }
        $this->renderView("login");
    }

    /**
     * Logout Action
     */
    public function logoutAction(){

        $this->disconnection($this->getUser()) ;
    }

    /**
     * add user
     */
    public function registerAction()
    {


        if (isset($_POST['email']) and isset($_POST['password']) and isset($_POST['crfToken'])) {

            if($_POST['crfToken'] == $this->crfToken("registre")) {
                $user = new User();
                $user->setEmail(htmlspecialchars(strip_tags($_POST['email'])));
                $user->setPassword(htmlspecialchars(strip_tags($_POST['password'])));
                List($this->status, $user) = $this->getReposotiry(UserRepository::class)->register($user);
                if ($this->status == "success") {
                    //   $this->redirectTo("");
                    $user->setStatus(1);
                    $user->setLastLogin(date("Y-m-d H:i:s"));
                    $this->connectUser($user);
                }
            }

        }
        $this->renderView("register");

    }

    public function logoutAllAction()
    {

        $this->getReposotiry(UserRepository::class)->logoutAll();

    }

    public function imConnectedAction()
    {
        $user = $this->getUser();
        $user->setLastLogin(date("Y-m-d H:i:s"));
        $user->setStatus(1);
        $this->getReposotiry(UserRepository::class)->updateUser($user);


    }
}