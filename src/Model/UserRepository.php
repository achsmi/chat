<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 21/04/2019
 * Time: 18:27
 */

namespace src\Model;


use src\Helper\HelperFactorie;
use PDO;

/**
 * Class UserRepository
 * @package src\Model
 */
class UserRepository
{

    private $_db;
    private $_statement;

    /**
     * User constructor.
     */
    public function __construct()
    {

        $this->_db = (new HelperFactorie())->databaseConnect();
    }

    /**
     * @param User $user
     * @return string
     */

    public function register(User $user)
    {

        if ($this->checkUserByEmail($user->getEmail())) return [ "le compte spécifié existe déja"  , $user];

        return $this->addUser($user);

    }


    public function find($id)
    {

        try {
            $this->_sql = "SELECT * FROM user WHERE id = :id";
            $this->_statement = $this->_db->prepare($this->_sql);
            $this->_statement->bindParam(':id', $id);

            $this->_statement->setFetchMode(PDO::FETCH_ASSOC);
            $this->_statement->execute();
            $row =  $this->_statement->fetch();

            $user = new User();
            $user->setId($row["id"]);
            $user->setEmail($row["email"]);
            $user->setStatus($row["status"]);
            $user->setLastLogin($row["lastLogin"]);

            return $user ;

        } catch (PDOException $e) {
            die('Error->checkUser() : ' . $e->getMessage());
        }

    }

    /**
     * @param User $user
     */
    public function login(User $user)
    {

        $findUser = $this->checkUser($user);
        if (!$findUser) return [ "l'email ou le mot de passe est incorrect"  , $user] ;
        $user->setCreatedAt($findUser["createdAt"]);
        $user->setId($findUser["id"]);
        $user->setStatus(1);
        $user->setLastLogin(date("Y-m-d H:i:s"));
        return ["success", $user];


    }

    public function checkUser(User $user)
    {

        try {
            $this->_sql = "SELECT * FROM user WHERE email = :email and password = :password";
            $this->_statement = $this->_db->prepare($this->_sql);
            $this->_statement->bindParam(':email', $user->getEmail());
            $this->_statement->bindParam(':password', md5($user->getPassword()));

            $this->_statement->setFetchMode(PDO::FETCH_ASSOC);
           // var_dump(md5($user->getPassword())) ;
           // var_dump($this->_statement) ;
            $this->_statement->execute();
            return $this->_statement->fetch();
        } catch (PDOException $e) {
            die('Error->checkUser() : ' . $e->getMessage());
        }


    }

    /**
     * @return array
     */
    public function liste()
    {
        try {
            $this->_sql = "SELECT * FROM user order By status desc";
            $this->_statement = $this->_db->prepare($this->_sql);

            $this->_statement->setFetchMode(PDO::FETCH_ASSOC);
            $this->_statement->execute();
            $arrayUsers = $this->_statement->fetchAll();

            $users = [];
            foreach ($arrayUsers as $row) {
                $user = new User();
                $user->setId($row["id"]);
                $user->setEmail($row["email"]);
                $user->setStatus($row["status"]);
                $user->setLastLogin($row["lastLogin"]);
                $users[] = $user;
            }
            return $users;
        } catch (PDOException $e) {
            die('Error->userListe() : ' . $e->getMessage());
        }
    }

    /**
     * @param User $user
     * @return string
     */
    public function addUser(User $user)
    {

        try {
            $this->_sql = "INSERT INTO user (email, password , createdAt , lastLogin , status) VALUES (:email, :password , :createdAt , :lastLogin , :status)";
            $this->_statement = $this->_db->prepare($this->_sql);
            $this->_statement->bindParam(':email', $user->getEmail());
            $this->_statement->bindParam(':password', md5($user->getPassword()));
            $this->_statement->bindParam(':createdAt', $user->getCreatedAt());
            $this->_statement->bindParam(':lastLogin', $user->getLastLogin());
            $this->_statement->bindParam(':status', $user->getStatus());
            $this->_statement->execute();
            $thisUser = $this->checkUserByEmail($user->getEmail()) ;
            $user->setId($thisUser[0]['id']) ;
            return ["success", $user];
        } catch (PDOException $e) {
            die('Error->addUser() : ' . $e->getMessage());
        }

    }

    public function updateUser($user)
    {
        try {
            $this->_sql = "UPDATE  user  SET   lastLogin = :lastLogin , status = :status where id = :id";
            $this->_statement = $this->_db->prepare($this->_sql);
            $this->_statement->bindParam(':lastLogin', $user->getLastLogin());
            $this->_statement->bindParam(':status', $user->getStatus());
            $this->_statement->bindParam(':id', $user->getId());
            $this->_statement->execute();
            return ["success", $user];
        } catch (PDOException $e) {
            die('Error->update() : ' . $e->getMessage());
        }

    }


    public function logoutAll(){
        try {
            $this->_sql = "UPDATE  user  SET  status = 0";
            $this->_statement = $this->_db->prepare($this->_sql);

            $this->_statement->execute();
            return ["success"];
        } catch (PDOException $e) {
            die('Error->update() : ' . $e->getMessage());
        }
    }
    /**
     * @param $email
     * @return array
     */
    public function checkUserByEmail($email)
    {

        try {
            $this->_sql = "SELECT * FROM user WHERE email = :email";
            $this->_statement = $this->_db->prepare($this->_sql);
            $this->_statement->bindParam(':email', $email);

            $this->_statement->setFetchMode(PDO::FETCH_ASSOC);
            $this->_statement->execute();
            return $this->_statement->fetchAll();
        } catch (PDOException $e) {
            die('Error->checkUser() : ' . $e->getMessage());
        }

    }

}