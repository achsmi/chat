<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 21/04/2019
 * Time: 20:25
 */

namespace src\Model;


use config\parameter;
use src\Helper\HelperFactorie;
use PDO ;
use src\Helper\Repository;

/**
 * Class MessageRepository
 * @package src\Model
 */
class MessageRepository
{

    private $_db;
    private $_statement;
    private $_numbre_of_display_message;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->reposotiry = new Repository() ;

        $this->_numbre_of_display_message = (new parameter())->_numbre_of_display_message ;
        $this->_db = (new HelperFactorie())->databaseConnect();
    }

    /**
     * @param Message $message
     * @return array
     */
    public function save(Message $message){
        try {
            $content=$message->getContent();
            $id=$message->getUser()->getId();
            $publicationDate=$message->getPublicationDate();
            $this->_sql = "INSERT INTO message (content,  user , publicationDate) VALUES (:content, :user , :publicationDate )";
            $this->_statement = $this->_db->prepare($this->_sql);
            $this->_statement->bindParam(':content', $content);
            $this->_statement->bindParam(':user',$id );
            $this->_statement->bindParam(':publicationDate', $publicationDate);
            $this->_statement->execute();
            return ["success", $message];
        } catch (PDOException $e) {
            die('Error->addUser() : ' . $e->getMessage());
        }
    }
    /**
     * @return array
     */
    public function liste(){
        try {
            $this->_sql = "SELECT * FROM message ORDER BY id DESC LIMIT ".$this->_numbre_of_display_message;
            $this->_statement = $this->_db->prepare($this->_sql);
            $this->_statement->setFetchMode(PDO::FETCH_ASSOC);
            $this->_statement->execute();

            $arrayMessage =  $this->_statement->fetchAll();

            $messages = [] ;
            foreach ($arrayMessage as $row) {
                $message = new Message();

                $message->setContent($row['content']) ;
                $message->setPublicationDate($row['publicationDate']) ;
                $user = $this->reposotiry->getManager(UserRepository::class)->find($row['user']) ;
                $message->setUser($user) ;
                $messages[] = $message ;
            }
            return $messages ;
        } catch (PDOException $e) {
            die('Error->messageListe() : ' . $e->getMessage());
        }


    }

    /**
     * @return array
     */
    public function all(){
        try {
            $this->_sql = "SELECT * FROM message ORDER BY id DESC ";
            $this->_statement = $this->_db->prepare($this->_sql);
            $this->_statement->setFetchMode(PDO::FETCH_ASSOC);
            $this->_statement->execute();

            $arrayMessage =  $this->_statement->fetchAll();

            $messages = [] ;
            foreach ($arrayMessage as $row) {
                $message = new Message();

                $message->setContent($row['content']) ;
                $message->setPublicationDate($row['publicationDate']) ;
                $user = $this->reposotiry->getManager(UserRepository::class)->find($row['user']) ;
                $message->setUser($user) ;
                $messages[] = $message ;
            }
            return $messages ;
        } catch (PDOException $e) {
            die('Error->messageListe() : ' . $e->getMessage());
        }
    }


}