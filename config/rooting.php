<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 21/04/2019
 * Time: 12:36
 */

namespace config;


class rooting
{

    /**
     * @var array
     * root List
     */
    private $root = [

        "" => ["controller" =>"User" , "action" => "login" ] ,
        "user_list" => ["controller" => "User", "action" => "index" ] ,
        "messages_list" => ["controller" => "Chat", "action" => "list" ] ,
        "registre" => ["controller" => "User", "action" => "register" ] ,
        "chat" => ["controller" => "Chat", "action" => "index" ] ,
        "sendMessage" => ["controller" => "Chat", "action" => "sendMessage" ] ,
        "getMessages" => ["controller" => "Chat", "action" => "getMessages" ] ,
        "getUsers" => ["controller" => "User", "action" => "getUsers" ] ,
        "logoutAll" => ["controller" => "User", "action" => "logoutAll" ] ,
        "imConnected" => ["controller" => "User", "action" => "imConnected" ] ,
        "logout" => ["controller" => "User", "action" => "logout" ] ,


    ] ;

    /**
     * @return mixed
     */
    public function generateRoute()
    {
        if(isset($_GET['url'])) {

            if(isset($this->root[$_GET['url']]) ) return $this->root[$_GET['url']] ;
            else   {
                // To do : create 404 page
                echo "404" ; die() ;
            }
        }
        else  {
        return $this->root[''] ;
    }
    }

}