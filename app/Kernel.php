<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 21/04/2019
 * Time: 11:12
 */

namespace app;

use config;
use src\Controller\ChatController;
use src\Controller\UserController;
use src\helper\url;

class Kernel
{


    /**
     * Kernel constructor.
     */
    public function __construct($env = null)
    {
        $this->autoload();
        if ($env == "web") {
            $this->configureRoutes();
        }
    }

    /**
     *
     */
    public function autoload()
    {

        define('SRC_DIR', 'src');
        define('APP_DIR', 'app');
        define('CONFIG_DIR', 'config');

        spl_autoload_extensions('.php');


        set_include_path(
            SRC_DIR . '/Controller' . PATH_SEPARATOR .
            SRC_DIR . '/Model' . PATH_SEPARATOR .
            SRC_DIR . '/Views' . PATH_SEPARATOR .
            SRC_DIR . '/Helper' . PATH_SEPARATOR .
            SRC_DIR . '/Listner' . PATH_SEPARATOR .
            APP_DIR . '/' . PATH_SEPARATOR .
            CONFIG_DIR . '/' . PATH_SEPARATOR .
            PATH_SEPARATOR . get_include_path()
        );

        spl_autoload_register();
    }

    /**
     *
     */

    public function configureRoutes()
    {
        $roting = new config\rooting();
        $root = $roting->generateRoute();
        if ($root['controller'] == "User") $object = new UserController();
        if ($root['controller'] == "Chat") $object = new ChatController();
        $function = $root['action'] . "Action";
        $object->$function();

    }

}